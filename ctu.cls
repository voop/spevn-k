\NeedsTeXFormat{LaTeX2e}[1994/06/01]
\ProvidesClass{ctu}

\LoadClass[a4paper,12pt,titlepage,oneside]{article}
\usepackage{a4wide}
\RequirePackage{graphicx}
\RequirePackage{color}
\RequirePackage{ifthen}
\RequirePackage{tabularx}
\RequirePackage{amsfonts}
\RequirePackage{amssymb}
\RequirePackage{amsmath}
\RequirePackage{indentfirst}
\RequirePackage{fancyhdr}
\RequirePackage{url}
\RequirePackage{float} % [H] option for env. (place [H]ere)
\RequirePackage{caption} %subfigure
\RequirePackage{subcaption} %subfigure
\RequirePackage{bm} %bold symbols in math env.   
\RequirePackage[bookmarks=true, hypertexnames=false, breaklinks=true, colorlinks=false, urlcolor=blue]{hyperref}  
\RequirePackage{pdfpages} %include pdf pages into our document
\RequirePackage{lastpage} %use for \LastPage reference
\RequirePackage{listings}
\RequirePackage{tocloft}

%Set default values
\newcommand\doclang{en}
\newcommand\doctype{report}
\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}

%Language options
\DeclareOption{cs}{
	\renewcommand\doclang{cs}
}
\DeclareOption{sk}{
	\renewcommand\doclang{sk}
}
\DeclareOption{en}{
	\renewcommand\doclang{en}
}
%Document types
\DeclareOption{bcthesis}{
	\renewcommand\doctype{bcthesis}
}
\DeclareOption{thesis}{
	\renewcommand\doctype{thesis}
}
\DeclareOption{report}{
	\renewcommand\doctype{report}
}
\DeclareOption*{}
\ProcessOptions\relax

%---------------------------------------
%-----------Common settings----------
%---------------------------------------
\pdfadjustspacing=1 
\frenchspacing
\setcounter{tocdepth}{2}
% no indent, free space between paragraphs
\setlength{\parindent}{0.5cm}
\setlength{\parskip}{1ex plus 0.5ex minus 0.2ex}
%fancy
\pagestyle{fancy}
\setlength{\headheight}{18pt}
\renewcommand{\footrulewidth}{0.4pt}
\rhead{\cctuTitle}
\cfoot{}
\fancypagestyle{plain}

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}
\lstset{language=C++, basicstyle=\footnotesize, showstringspaces=false,
frame=single, breaklines=true, breakatwhitespace=true, numbers=left,
stepnumber=1, backgroundcolor=\color{white}, rulecolor=\color{black}, tabsize=2,
captionpos=b, title=\lstname, keywordstyle=\color{blue}\bfseries,
stringstyle=\color{mauve}, commentstyle=\color{dkgreen}}
\renewcommand\lstlistingname{Algorithm}
\renewcommand\lstlistlistingname{List of Algorithms}
\renewcommand{\sectionmark}[1]{\markboth{{\thesection~\hspace{0.5em}\MakeUppercase{#1}}}{}}

\newlistof{appendices}{apc}{\cctulistappendicesname}
\renewcommand{\theappendices}{\Alph{appendices}}
\renewcommand{\cftapctitlefont}{\cftloftitlefont}
\renewcommand{\cftbeforeapctitleskip}{\cftbeforeloftitleskip}
\renewcommand{\cftafterapctitleskip}{\cftafterloftitleskip} 

\newlistof{todos}{todo}{\cctulisttodosname}
\renewcommand{\cfttodotitlefont}{\cftloftitlefont}
\renewcommand{\cftbeforetodotitleskip}{\cftbeforeloftitleskip}
\renewcommand{\cftaftertodotitleskip}{\cftafterloftitleskip}

\AtBeginDocument{
	\ctuInit
}
\AtEndDocument{
	\clearpage
	\addtocounter{page}{-1}
	\immediate\write\@auxout{\string\newlabel{cctuLastPage}{{}{\thepage}{}{}}}
	\immediate\write\@auxout{\string\newlabel{cctuLastFigure}{{}{\thefigure}{}{}}}
	\immediate\write\@auxout{\string\newlabel{cctuLastTable}{{}{\thetable}{}{}}}
	\immediate\write\@auxout{\string\newlabel{cctuLastAlgorithm}{{}{\thelstlisting}{}{}}}	
	\immediate\write\@auxout{\string\newlabel{cctuLastAppendix}{{}{\theappendices}{}{}}}
	\addtocounter{page}{1}
}

\makeatletter
\@addtoreset{section}{part}
\makeatother  

%---------------------------------------
%-----------Internal variables----------
%---------------------------------------
\newcommand{\cctuTitle}{}
\newcommand{\cctuAuthor}{}
\newcommand{\cctuKeywords}{}
\newcommand{\cctuSupervisor}{}
\newcommand{\cctuSupervisorHead}{Thesis supervisor:}
\newcommand{\cctuInstitution}{CZECH TECHNICAL UNIVERSITY IN PRAGUE}
\newcommand{\cctuFaculty}{Faculty of Electrical Engineering}
\newcommand{\cctuDepartment}{Department of Cybernetics}
\newcommand{\cctuDocVersion}{0.1}
\newcommand{\cctuDocType}{}
\newcommand{\cctuDeclarationBody}{
	I hereby declare that I have completed this thesis independently and that I have listed all used information sources in accordance with Methodical instruction about ethical principles in the preparation of university theses.}
\newcommand{\cctuDeclarationHead}{Declaration}
\newcommand{\cctuDeclarationFoot}{In Prague on}
\newcommand{\cctuAcknowledgementsBody}{I would like to thank my supervisor \cctuSupervisor~for his support and encouragement during this project.}
\newcommand{\cctuAcknowledgementsHead}{Acknowledgements}
\newcommand{\cctuAbstraktBody}{}
\newcommand{\cctuAbstractBody}{}
\newcommand{\cctuLogoPath}{fig/cvutLogo123654.png}
\newcommand{\cctulistappendicesname}{List of Appendices}
\newcommand{\cctuAppendixName}{Appendix}
\newcommand{\cctuAppendicesName}{Appendices}
\newcommand{\cctulisttodosname}{Todo List}

%---------------------------------------
%---------Commands declaration-----------
%---------------------------------------
\newcommand{\ctuAuthor}[1]{\renewcommand{\cctuAuthor}{#1}}
\newcommand{\ctuTitle}[1]{\renewcommand{\cctuTitle}{#1}}
\newcommand{\ctuKeywords}[1]{\renewcommand{\cctuKeywords}{#1}}
\newcommand{\ctuSupervisor}[1]{\renewcommand{\cctuSupervisor}{#1}}
\newcommand{\ctuInstitution}[1]{\renewcommand{\cctuInstitution}{#1}}
\newcommand{\ctuFaculty}[1]{\renewcommand{\cctuFaculty}{#1}}
\newcommand{\ctuDepartment}[1]{\renewcommand{\cctuDepartment}{#1}}
\newcommand{\ctuDocVersion}[1]{\renewcommand{\cctuDocVersion}{#1}}
\newcommand{\ctuDocType}[1]{\renewcommand{\cctuDocType}{#1}}
\newcommand{\ctuDeclaration}[1]{\renewcommand{\cctuDeclarationBody}{#1}}
\newcommand{\ctuAcknowledgements}[1]{\renewcommand{\cctuAcknowledgementsBody}{#1}}
\newcommand{\ctuAbstrakt}[1]{\renewcommand{\cctuAbstraktBody}{#1}}
\newcommand{\ctuAbstract}[1]{\renewcommand{\cctuAbstractBody}{#1}}
\newcommand{\ctuInit}{}
\newcommand{\ctuStartPages}{}
\newcommand{\ctuTitlePage}{}
\newcommand{\ctuTablesOfContents}{}
\newcommand{\ctuLogoPath}[1]{\renewcommand{\cctuLogoPath}{#1}}

%Initialize pdf output settings
\renewcommand{\ctuInit}{
	%PDF output settings
	\hypersetup{  
	pdfauthor={\cctuAuthor},
	pdftitle={\cctuTitle},
	pdfsubject={\cctuTitle},
	pdfkeywords={\cctuKeywords}
	}
}
 %Call this cmd when want to restart fancy style to default
\newcommand{\cctuFancyText}{
	\pagestyle{fancy}
	\pagenumbering{arabic}
	\cfoot{}
	\rfoot{\thepage$/$\pageref{LastPage}}
	\lhead{\leftmark}
}

%Doctype specific commands
\ifthenelse{\equal{\doctype}{bcthesis}}{
	\ctuDocType{BACHELOR THESIS}
}{}
\ifthenelse{\equal{\doctype}{thesis}}{
	\ctuDocType{DIPLOMA THESIS}
}{}
\ifthenelse{\equal{\doctype}{report}}{
	\ctuDocType{TECHNICAL REPORT}
}{}

%---------------------------------------
%-----------Czech-----------------------
%---------------------------------------
\ifthenelse{\equal{\doclang}{cs}}{
	\RequirePackage[czech]{babel}
	\RequirePackage[T1]{fontenc}
	\ctuInstitution{ČESKÉ VYSOKÉ UČENÍ TECHNICKÉ V~PRAZE}
	\ctuFaculty{Fakulta elektrotechnická}
	\ctuDepartment{Katedra kybernetiky}
	\ifthenelse{\equal{\doctype}{bcthesis}}{
	\ctuDocType{BAKALÁŘSKÁ PRÁCE}
	}{}
	\ifthenelse{\equal{\doctype}{thesis}}{
		\ctuDocType{DIPLOMOVÁ PRÁCE}
	}{}
	\ifthenelse{\equal{\doctype}{report}}{
		\ctuDocType{TECHNICKÁ ZPRÁVA}
	}{}
	\ctuDeclaration{Prohlašuji, že jsem předloženou práci vypracoval samostatně a že jsem uvedl veškeré použité informační zdroje v souladu s Metodickým pokynem o dodržování etických principů při přípravě vysokoškolských závěrečných prací.}
	\renewcommand{\cctuDeclarationHead}{Prohlášení autora práce}
	\renewcommand{\cctuDeclarationFoot}{V Praze dne}
	\renewcommand{\cctuAcknowledgementsBody}{Rád bych zde poděkoval vedoucímu bakalářské práce za jeho rady a čas, který mi věnoval při řešení dané problematiky.}
	\renewcommand{\cctuAcknowledgementsHead}{Poděkování}
	\renewcommand{\cctuSupervisorHead}{Vedoucí práce:}
	\renewcommand\lstlistingname{Algortimus}
	\renewcommand\lstlistlistingname{Seznam algoritmů}
	\renewcommand{\cctulistappendicesname}{Seznam příloh}
	\renewcommand{\cctuAppendixName}{Příloha}
	\renewcommand{\cctuAppendicesName}{Přílohy}
}{}
%---------------------------------------
%-----------Slovak----------------------
%---------------------------------------
\ifthenelse{\equal{\doclang}{sk}}{
	\RequirePackage[english,slovak]{babel}
	%\RequirePackage[IL2]{fontenc}
	\ctuInstitution{ČESKÉ VYSOKÉ UČENÍ TECHNICKÉ V~PRAZE}
	\ctuFaculty{Fakulta elektrotechnická}
	\ctuDepartment{Katedra kybernetiky}
	\ifthenelse{\equal{\doctype}{bcthesis}}{
	\ctuDocType{BAKALÁRSKA PRÁCA}
	}{}
	\ifthenelse{\equal{\doctype}{thesis}}{
		\ctuDocType{DIPLOMOVÁ PRÁCA}
	}{}
	\ifthenelse{\equal{\doctype}{report}}{
		\ctuDocType{TECHNICKÁ SPRÁVA}
	}{}	
	\ctuDeclaration{Prehlasujem, že som predloženú prácu vypracoval samostatne a že som uviedol všetky použité informačné zdroje v súlade s Metodickým pokynom o dodržiavanie etických princípov pri príprave vysokoškolských záverečných prác.}
	\renewcommand{\cctuDeclarationHead}{Prehlásenie autora práce}
	\renewcommand{\cctuDeclarationFoot}{V Prahe dňa}
	\renewcommand{\cctuAcknowledgementsBody}{Rád by som poďakoval vedúcemu bakalárskej práce za jeho rady a čas, ktorý mi venoval pri riešení danej problematiky.}
	\renewcommand{\cctuAcknowledgementsHead}{Poďakovanie}
	\renewcommand{\cctuSupervisorHead}{Vedúci práce:}
	\renewcommand\lstlistingname{Algortimus}
	\renewcommand\lstlistlistingname{Zoznam algoritmov}
	\renewcommand{\cctulistappendicesname}{Zoznam príloh}
	\renewcommand{\cctuAppendixName}{Príloha}
	\renewcommand{\cctuAppendicesName}{Prílohy}
}{}



%---------------------------------------
%-----------Helpers---------------------
%---------------------------------------
\newcommand{\ctuSummary}{
	\begin{table}[H]
	\begin{tabular}{rl}
		\textbf{Variable name}  & \textbf{Value}\\
		\hline
		  Document type:&\doctype\\     
		  Document lang:&\doclang\\
		  Title:&\cctuTitle\\
		  Author:&\cctuAuthor\\
		  Keywords:&\cctuKeywords\\ 
		  Supervisor:&\cctuSupervisor\\
		  Faculty:&\cctuFaculty\\
		  Institution:&\cctuInstitution\\
		  Department:&\cctuDepartment\\
		  Document version:&\cctuDocVersion\\
		  Document type:&\cctuDocType\\
	\end{tabular}
	\caption{Summary of settings}
	\end{table}
}
\newcommand{\ctuHelp} {
	%Print available commands and their descritptions
	\begin{table}[H]
	\begin{tabular}{rcl}
		\textbf{Commands}  & \textbf{Param} &\textbf{Description}\\
		\hline
		  \textbackslash ctuHelp&0&Show this help\\
		  \textbackslash ctuSummary&0&Print actual settings\\
		  \textbackslash ctuInit&0&Init pdf (set title, keywords, subject and author tags)\\
		  \textbackslash ctuTitle&1&\\
		  \textbackslash ctuAuthor&1&\\
		  \textbackslash ctuKeywords&1&\\
		  \textbackslash ctuSupervisor&1&\\
		  \textbackslash ctuFaculty&1&\\
		  \textbackslash ctuInstitution&1&\\
		  \textbackslash ctuDepartment&1&\\
		  \textbackslash ctuDocVersion&1&\\
		  \textbackslash ctuDocType&1& change type of document\\
	\end{tabular}
	\caption{Help}
	\end{table}	
}
%---------------------------------------
%-----------Title page------------------
%---------------------------------------
\renewcommand{\ctuTitlePage}{
	\pagestyle{empty}
	\begin{center}
	
	\cctuInstitution
	\vskip 10pt
	
	\vskip 8pt
	\cctuFaculty
	 
	\vspace{50pt}
	\ifthenelse{\equal{\cctuDocType}{}}{}{
		{\Huge\bf \cctuDocType}\\
	}
	\vspace{40pt}
	\IfFileExists{\cctuLogoPath}{
		\includegraphics[height=8cm]{\cctuLogoPath}
	}{
	\vspace{8cm}
	}
	
	
	\vspace{40pt}
	\ifthenelse{\equal{\cctuAuthor}{}}{}{
		{\Large\rm \cctuAuthor} \\
	}
	\vspace{20pt}
	{\Large\bf \cctuTitle} 
	
	
	\vspace{60pt}
	\ifthenelse{\equal{\cctuDepartment}{}}{}{
		{\bf \cctuDepartment}\\
	}
	\vspace{5pt}   
	\ifthenelse{\equal{\cctuSupervisor}{}}{}{
		{\cctuSupervisorHead~{\bf \cctuSupervisor}}
	}
	\vspace{30pt}
	\end{center}
	\newpage{}
	\cctuFancyText
}

%---------------------------------------
%-----------Start pages-----------------
%---------------------------------------
\newcommand{\cctuDeclaration}{
	\ifthenelse{\equal{\cctuDeclarationBody}{}}{}{
		~\vfill{} \section*{\cctuDeclarationHead}
		\cctuDeclarationBody
		\vspace{1.5cm}
		~\\
		\cctuDeclarationFoot .............................\hfill{}...............................................\\
		\vspace{2.5cm} \newpage{}
	}
}
\newcommand{\cctuAcknowledgements}{
	\ifthenelse{\equal{\cctuAcknowledgementsBody}{}}{}{
		~\vfill{}
		\section*{\cctuAcknowledgementsHead}
			\cctuAcknowledgementsBody
		\newpage{}
	}
}
\newcommand{\cctuAbstrakt}{
	\ifthenelse{\equal{\cctuAbstraktBody}{}}{}{
		\vfill
		\begin{center}
		{\it \large Abstrakt}
		
		\vspace{0.2cm}
		
		\begin{minipage}{0.8\textwidth}{\cctuAbstraktBody}
		\end{minipage}
		\end{center}
		\vfill
		\vspace{1cm}
		\ifthenelse{\equal{\cctuAbstractBody}{}}{
		\newpage{}}{}
	}
}
\newcommand{\cctuAbstract}{
	\ifthenelse{\equal{\cctuAbstractBody}{}}{}{
		\begin{center}
		{\it \large Abstract}
		
		\vspace{0.2cm}	
		
		\begin{minipage}{0.8\textwidth}{\cctuAbstractBody}
		\end{minipage}
		\end{center}
		\vfill
		\newpage{}
	}
}

\renewcommand{\ctuStartPages}{ 
	\pagestyle{empty}
	\cctuDeclaration
	\cctuAcknowledgements
	\cctuAbstrakt
	\cctuAbstract
	\cctuFancyText
}
%---------------------------------------
%-----------Tables of contents----------
%---------------------------------------
\renewcommand{\ctuTablesOfContents}{
	\pagestyle{fancy}
	\pagenumbering{roman}
	\cfoot{\thepage}
	\rfoot{}
	\tableofcontents
	\pagebreak
	
	\ifthenelse{\equal{\pageref{cctuLastFigure}}{0}}{}{
		\listoffigures
		\pagebreak
	}
	\ifthenelse{\equal{\pageref{cctuLastTable}}{0}}{}{	
		\listoftables
		\pagebreak
	}
	\ifthenelse{\equal{\pageref{cctuLastAlgorithm}}{0}}{}{
		\lstlistoflistings
		\pagebreak
	}	
	\ifthenelse{\equal{\pageref{cctuLastAppendix}}{}}{}{
		\listofappendices
		\pagebreak
	}	
	\cctuFancyText	
}

%---------------------------------------
%-----------Appendices------------------
%---------------------------------------
\newcommand{\ctuAppendix}[1]{
	\ifthenelse{\equal{\arabic{appendices}}{0}}{
		\phantomsection 
		\addcontentsline{toc}{section}{\cctuAppendicesName}
	}{}	
	\refstepcounter{appendices}
	\markboth{\MakeUppercase{\cctuAppendixName~\theappendices}}{}
	\noindent\textbf{\Large#1}
	
	\addcontentsline{apc}{figure}{\protect\numberline{\theappendices}#1}
}

%---------------------------------------
%-----------TODO  Lists-----------------
%---------------------------------------
\newcommand{\ctuTodo}[1]{
	\refstepcounter{todos}
	\addcontentsline{todo}{figure}{\protect\numberline{\thetodos}#1}
} 
\newcommand{\ctuPrintTodos}{
	\phantomsection 
	\addcontentsline{toc}{section}{\cctulisttodosname}		
	\listoftodos
	\clearpage
} 
